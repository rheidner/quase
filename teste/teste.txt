classe _Gato filha da classe _Animal & classe _Cachorro filha da classe _Animal;

classe _Animal comeca

	var real peso;
	var real altura;

	funcao real getPeso	() peso

	funcao real getAlctura () altura

	funcao real pesoVezesAltura (int nome, int peso) peso * altura

	procedimento setPeso (real peso_aux) comeca 
		peso = peso_aux;
	termina

	procedimento setAltura (real altura_aux) comeca 
		altura = altura_aux;		
	termina

termina

classe _Gato comeca
	var real agressividade;
	var bool tem_raca;

	procedimento setTemRaca (bool raca_aux) comeca
		tem_raca = raca_aux;
	termina

	procedimento setAgressividade (real aggr_aux) comeca
		agressividade = aggr_aux;
	termina	

	funcao bool getTemRaca () tem_raca

	funcao real getAgressividade () agressividade
termina

classe _Cachorro comeca
	var real lealdade;
	var bool tem_raca;

	procedimento setTemRaca (bool raca_aux) comeca
		tem_raca = raca_aux;
	termina

	procedimento setLealdade (real leal_aux) comeca
		lealdade = leal_aux;
	termina

	funcao bool getTemRaca () tem_raca

	funcao real getLealdade () lealdade
termina

classe _Principal comeca
	
	objeto _IO io;

	=> procedimento principal () comeca
		objeto _Cachorro rex;
		objeto _Gato bruno;
		
		rex.setPeso(10);
		rex.setAltura(0.30);
		io.print(rex.pesoVezesAltura());	
	
		se(4.0 < 5.0)
			enquanto (true)
					se (true)
						se (true) comeca termina
                    senao comeca termina
		    
						senao comeca termina
		   se (true) comeca termina
                      
        
		bruno.setPeso(3);
		bruno.setAltura(0.15);
		io.print(bruno.pesoVezesAltura());		

		rex.setLealdade(50);
		bruno.setAgressividade(100);

		io.print(rex.getLealdade() < bruno.setAgressividade());
	termina

termina