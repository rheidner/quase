package quase;

import quase.node.TId;

public class NoVariableFoundException extends Exception {
	
	public NoVariableFoundException(TId id) {
		this("Vari�vel "+id + " [" + id.getLine()+ "," + id.getPos() + "] " +" n�o foi declarada");
	}

	public NoVariableFoundException(String message) {
		super(message);
	}

}
