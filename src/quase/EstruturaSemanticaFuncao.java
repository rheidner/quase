package quase;

import java.util.List;
import java.util.Map;

import quase.node.PTipo;
import quase.node.TId;

public class EstruturaSemanticaFuncao extends EstruturaSemanticaMetodo {
	private PTipo retorno;
	private List<Bloco> blocos;
	
	public EstruturaSemanticaFuncao adicionaParametros(List<Variavel> vars) {
		for(Variavel var: vars)
			super.adicionaParametro(var);
		
		return this;
	}
	
	public EstruturaSemanticaFuncao(TId id, List<PTipo> tipo_parametros, PTipo retorno) {
		super(id, tipo_parametros);
		this.retorno = retorno;
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public PTipo buscaVariavel(TId id) {
		if(parametros.containsKey(id)) {
			return parametros.get(id).getTipo();
		}
		return null;
	}
	
	@Override
	public void imprime(int indent) {
		imprimeIndent(indent);
		System.out.println("Fun��o:");
		imprimeIndent(indent+1);
		System.out.println("Id: "+ id+"\n");
		imprimeParametros(indent+1);
		imprimeIndent(indent+1);
		System.out.println("Retorno: "+retorno);
		
		
	}
	
	public PTipo getRetorno() {
		return retorno;
	}

	public void setRetorno(PTipo retorno) {
		this.retorno = retorno;
	}

	public List<Bloco> getBlocos() {
		return blocos;
	}

	public void setBlocos(List<Bloco> blocos) {
		this.blocos = blocos;
	}

	@Override
	protected String getName() {
		return this.id.getText();
	}

}
