package quase;

import quase.node.TId;

public class NoFunctionFoundException extends Exception {
	public NoFunctionFoundException(TId id) {
		this("Fun��o "+id+" n�o foi declarada");
	}

	public NoFunctionFoundException(String message) {
		super(message);
	}
}
