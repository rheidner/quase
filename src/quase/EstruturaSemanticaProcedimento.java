package quase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import quase.node.PTipo;
import quase.node.TId;

public class EstruturaSemanticaProcedimento extends EstruturaSemanticaMetodo{
	
	private Map<String, Variavel> variaveis;
	private Map<String, Constante> constantes;
	private List<Bloco> bloco;
	
	public EstruturaSemanticaProcedimento(TId id, List<PTipo> tipo_parametros) {
		super(id, tipo_parametros);

		this.variaveis = new HashMap<String, Variavel>();
		this.constantes = new HashMap<String, Constante>();
	}
	
	public EstruturaSemanticaProcedimento adicionaVariavel(Variavel var) {
		variaveis.put(var.getId().getText(), var);
		return this;
	}

	@Override
	public PTipo buscaVariavel(TId id) {
		String nome = id.getText();
		
		if(variaveis.containsKey(nome)) {
			return variaveis.get(nome).getTipo();
		} else if(constantes.containsKey(nome)) {
			return constantes.get(nome).getTipo();
		} else if(parametros.containsKey(nome)) {
			return parametros.get(nome).getTipo();
		}
		
		return null;
	}
	
	
	public EstruturaSemanticaProcedimento adicionaVariaveis(List<Variavel> vars) {
		for(Variavel var: vars)
			adicionaVariavel(var);
		return this;
	}
	
	public EstruturaSemanticaProcedimento adicionaConstante(Constante cons) {
		constantes.put(cons.getId().getText(), cons);
		return this;
	}
	
	public EstruturaSemanticaProcedimento adicionaConstantes(List<Constante> conss) {
		for(Constante cons: conss)
			adicionaConstante(cons);
		return this;
	}
	
	public EstruturaSemanticaProcedimento adicionaParametros(List<Variavel> vars) {
		for(Variavel var: vars)
			super.adicionaParametro(var);
		
		return this;
	}
	
	private void imprimeVariaveis(int indent) {
		imprimeIndent(indent);
		System.out.println("Variaveis:");
		for(String key: variaveis.keySet()) {
			imprimeIndent(indent+1);
			System.out.println("Id: "+key+" Tipo: "+variaveis.get(key).getTipo());
		}
		System.out.println();
	}
	
	@Override
	public void imprime(int indent) {
		imprimeIndent(indent);
		System.out.println("Procedimento:");
		imprimeIndent(indent+1);
		System.out.println("Id: "+ id+"\n");
		imprimeParametros(indent+1);
		imprimeVariaveis(indent+1);
	
	}
	
	@Override
	protected String getName() {
		return this.id.getText();
	}
	
}
