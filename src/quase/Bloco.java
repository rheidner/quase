package quase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import quase.node.PTipo;
import quase.node.TId;

public class Bloco extends TiposID {
	private Map<TId, Variavel> variaveis;
	private Map<TId, Constante> constantes;
	private List<Bloco> bloco;
	
	public Bloco() {
		this.variaveis = new HashMap<TId, Variavel>();
		this.constantes = new HashMap<TId, Constante>();
	}
	
	@Override
	public PTipo buscaVariavel(TId id) {
		if(variaveis.containsKey(id)) {
			return variaveis.get(id).getTipo();
		} else if(constantes.containsKey(id)) {
			return constantes.get(id).getTipo();
		}
		
		return null;
	}
	
	public List<Bloco> getBloco() {
		return bloco;
	}
	public void setBloco(List<Bloco> bloco) {
		this.bloco = bloco;
	}
	public Map<TId, Variavel> getVariaveis() {
		return variaveis;
	}
	public void setVariaveis(Map<TId, Variavel> variaveis) {
		this.variaveis = variaveis;
	}
	public Map<TId, Constante> getConstantes() {
		return constantes;
	}
	public void setConstantes(Map<TId, Constante> constantes) {
		this.constantes = constantes;
	}
	
	public Bloco adicionaVariavel(Variavel var) {
		variaveis.put(var.getId(), var);
		return this;
	}
	
	public Bloco adicionaVariaveis(List<Variavel> vars) {
		for(Variavel var: vars)
			adicionaVariavel(var);
		return this;
	}
	public Bloco adicionaConstante(Constante cons) {
		constantes.put(cons.getId(), cons);
		return this;
	}
	
	public Bloco adicionaConstantes(List<Constante> cons) {
		for(Constante con: cons)
			adicionaConstante(con);
		return this;
	}

	@Override
	protected String getName() {
		return "Bloco";
	}
	
	
}
