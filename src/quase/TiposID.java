package quase;

import quase.node.PTipo;
import quase.node.TId;

public abstract class TiposID {
	protected abstract String getName();
	protected abstract PTipo buscaVariavel(TId id);
}
