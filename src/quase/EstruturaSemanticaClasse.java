package quase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import quase.node.PTipo;
import quase.node.TCid;
import quase.node.TId;

public class EstruturaSemanticaClasse extends TiposID{
	private TCid id;
	private Map<String, Variavel> variaveis;
	private Map<String, Constante> constantes;
	
	private Map<String, EstruturaSemanticaMetodo> tabela_metodo;
	
	public EstruturaSemanticaClasse(TCid id) {
		this.variaveis = new HashMap<String, Variavel>();
		this.constantes = new HashMap<String, Constante>();
		this.tabela_metodo = new HashMap<String, EstruturaSemanticaMetodo>();
		this.id = id;
	}
	
	public Map<String, EstruturaSemanticaMetodo> getTabela_metodo() {
		return tabela_metodo;
	}
	
	public PTipo buscaVariavel(TId id) {
		
		String nome = id.getText();
		
		if(variaveis.containsKey(nome)) {
			return variaveis.get(nome).getTipo();
		} else if(constantes.containsKey(nome)) {
			return constantes.get(nome).getTipo();
		}
		
		return null;
	}
	
	public EstruturaSemanticaFuncao buscaFuncao(TId id) {
		String nome = id.getText();
		
		if(tabela_metodo.containsKey(nome) && tabela_metodo.get(nome) instanceof EstruturaSemanticaFuncao){
			return ((EstruturaSemanticaFuncao) tabela_metodo.get(nome));
		}
		
		return null;
	}
	
	public EstruturaSemanticaClasse setTabela_metodo(Map<String, EstruturaSemanticaMetodo> tabela_metodo) {
		this.tabela_metodo = tabela_metodo;
		return this;
	}
	
	public EstruturaSemanticaClasse adicionaMetodo(TId id, EstruturaSemanticaMetodo est) {
		this.tabela_metodo.put(id.getText(), est);
		return this;
	}
	public EstruturaSemanticaClasse adicionaMetodos(Map<TId, EstruturaSemanticaMetodo> tabela){
		for(TId key: tabela.keySet())
			adicionaMetodo(key, tabela.get(key));
		
		return this;
	}
	
	public EstruturaSemanticaClasse adicionaVariavel(Variavel var) {
		variaveis.put(var.getId().getText(), var);
		return this;
	}
	
	public EstruturaSemanticaClasse adicionaVariaveis(List<Variavel> vars) {
		for(Variavel var: vars)
			adicionaVariavel(var);
		return this;
	}
	
	public EstruturaSemanticaClasse adicionaConstante(Constante cons) {
		constantes.put(cons.getId().getText(), cons);
		return this;
	}
	
	public EstruturaSemanticaClasse adicionaConstantes(List<Constante> conss) {
		for(Constante cons: conss)
			adicionaConstante(cons);
		return this;
	}
	
	public void imprimeIndent(int indent) {
		for(int i = 0; i < indent; i++) 
			System.out.print("\u0009");
	}
	
	public void imprime() {
		int indent = 1;
		
		System.out.println("Classe: "+id);
		imprimeIndent(indent);
		System.out.println("Atributos:");
		
		for(String key: variaveis.keySet()) {
			imprimeIndent(indent+1);
			System.out.println("Id: "+key+" Tipo: "+variaveis.get(key).getTipo());
		}
		
		for(String key: constantes.keySet()) {
			imprimeIndent(indent+1);
			System.out.println("Id: "+key+" Tipo: "+constantes.get(key).getTipo());
		}
		
		System.out.print("\u0009");
		System.out.println("Metodos:");
		
		for(String key: tabela_metodo.keySet()) {
			tabela_metodo.get(key).imprime(indent+1);
			System.out.println();
		}
	}

	@Override
	protected String getName() {
		return this.id.getText();
	}
	
}
