package quase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import quase.analysis.DepthFirstAdapter;
import quase.node.*;

public class Semantico extends DepthFirstAdapter{
	
	private static LinkedList<TiposID> escopos;
	
	@Override
	public void inStart(Start node)
	    {  
		   escopos = new LinkedList<TiposID>();
		   System.out.println("-------------------------------------------------");
		   System.out.println("Iniciando an�lise sem�ntica...");
		   System.out.println("-------------------------------------------------");
	    }
	
	 @Override
	 public void outStart(Start node)
	    {
		    System.out.println("-------------------------------------------------");
	        System.out.println("Fim da an�lise sem�ntica");
	        System.out.println("-------------------------------------------------");
			  
	    }
	 
	 @Override
	 public void inASeComando(ASeComando node) {
	 	PExpr exp = node.getExpr();
	 	
	 	if(exp instanceof AMenorExpr) {
	 		AMenorExpr aExpr = (AMenorExpr) exp;
	 		PExpr esq = aExpr.getEsq();
	 		PExpr dir = aExpr.getDir();
	 		
	 		//System.out.println(getTipo(dir).getClass());
	 		
	 		PTipo tipoEsq = getTipo(esq);
	 		PTipo tipoDir = getTipo(dir);
	 		
	 		if(tipoEsq instanceof ATipoPrimitivoTipo && tipoDir instanceof ATipoPrimitivoTipo) {
	 			ATipoPrimitivoTipo primitivoEsq = (ATipoPrimitivoTipo) tipoEsq;
	 			ATipoPrimitivoTipo primitivoDir = (ATipoPrimitivoTipo) tipoDir;
	 			
	 			if (! primitivoEsq.getTipoPrimitivo().getClass().equals(primitivoDir.getTipoPrimitivo().getClass())) {
	 				System.out.println("erro");
	 			} else if (primitivoEsq.getTipoPrimitivo() instanceof ABoolTipoPrimitivo){
	 				System.out.println("erro");
	 			}
	 		} else {
	 			System.out.println("erro");
	 		}
	 	}
	 	
	 	if (exp instanceof AIgualExpr) {
	 		AMenorExpr aExpr = (AMenorExpr) exp;
	 		PExpr esq = aExpr.getEsq();
	 		PExpr dir = aExpr.getDir();
	 		
	 		PTipo tipoEsq = getTipo(esq);
	 		PTipo tipoDir = getTipo(dir);
	 		
	 		if(tipoEsq instanceof ATipoPrimitivoTipo && tipoDir instanceof ATipoPrimitivoTipo) {
	 			ATipoPrimitivoTipo primitivoEsq = (ATipoPrimitivoTipo) tipoEsq;
	 			ATipoPrimitivoTipo primitivoDir = (ATipoPrimitivoTipo) tipoDir;
	 			
	 			if (! primitivoEsq.getTipoPrimitivo().getClass().equals(primitivoDir.getTipoPrimitivo().getClass())) {
	 				System.out.println("erro");
	 			}
	 		} else {
	 			System.out.println("erro");
	 		}
	 	}
	 }
	 
	 private PTipo getTipo(PExpr expr) {
		 if(expr instanceof AAtributoSemIdExpr) {
			 AAtributoSemIdExpr aExpr = (AAtributoSemIdExpr) expr;
			 
			 try {
				 PTipo retorno = buscaVariavel(aExpr.getId());
				 return retorno;
			 } catch(NoVariableFoundException ex) {
				 ex.printStackTrace();
			 }
			 
		 } else if(expr instanceof AInteiroExpr) {
			 return new ATipoPrimitivoTipo(new AIntTipoPrimitivo(new TInt()));
		 } else if(expr instanceof ARealExpr) {
			 return new ATipoPrimitivoTipo(new ARealTipoPrimitivo(new TReal()));
		 } else if(expr instanceof AVerdadeExpr || expr instanceof AFalsoExpr) {
			 return new ATipoPrimitivoTipo(new ABoolTipoPrimitivo());
		 } else if(expr instanceof AChamadaSemIdExpr) {
			 AChamadaSemIdExpr aExpr = (AChamadaSemIdExpr) expr;
			 AChamadaExpr cExpr = (AChamadaExpr) aExpr.getExpr();
			 
			 try {
				 EstruturaSemanticaFuncao funcao = buscaFuncao(cExpr.getId());
				 List<PTipo> tipos = new ArrayList<>(funcao.getTipos_parametros().size());
				 List<PTipo> params = funcao.getTipos_parametros();
				 
				 for(PExpr exp: cExpr.getExpr()) {
					 tipos.add(getTipo(exp));
				 }
				 
				 if(!validaTiposParametros(tipos, params))
					 System.out.println("erro");
				 
				 return funcao.getRetorno();
				 
			 } catch(NoFunctionFoundException ex) {
				 ex.printStackTrace();
			 }
		 } else if (expr instanceof AExprChamadaExpr){
			 
			 if (((AExprChamadaExpr) expr).getExpr() instanceof AAtributoSemIdExpr) {
				 AAtributoSemIdExpr aExpr = (AAtributoSemIdExpr) ((AExprChamadaExpr) expr).getExpr();
				 
				 try {
					 PTipo retorno = buscaVariavel(aExpr.getId());
					 return retorno;
				 } catch(NoVariableFoundException ex) {
					 System.out.println(ex.getMessage());
					 System.exit(0);
				 }
			 } else if ( ((AExprChamadaExpr) expr).getExpr() instanceof AChamadaComIdExpr) {
				 AChamadaComIdExpr aExpr = (AChamadaComIdExpr) ((AExprChamadaExpr) expr).getExpr();
				 
				 try {
					 PTipo retorno = buscaVariavel(aExpr.getId());
					 return retorno;
				 } catch(NoVariableFoundException ex) {
					 System.out.println(ex.getMessage());
					 System.exit(0);
				 }
			 }
			 
		 }
		 return null;
	 }
	 
	 
	 
	 private boolean validaTiposParametros(List<PTipo> exprs, List<PTipo> params) {
		 for(int i = 0; i < exprs.size(); i++) {
			 if(exprs.get(i) != params.get(i)) return false;
		 }
		 return true;
	 }
	 private EstruturaSemanticaFuncao buscaFuncao(TId id) throws NoFunctionFoundException {
		 for(int i = 0; i < escopos.size(); i++) {
			 if(escopos.get(i) instanceof EstruturaSemanticaClasse) {
				 EstruturaSemanticaClasse escopoClasse = ((EstruturaSemanticaClasse) escopos.get(i));
				 EstruturaSemanticaFuncao escopoFuncao = escopoClasse.buscaFuncao(id);
				 if(escopoFuncao != null) return escopoFuncao;
			 }
		 }
		 
		 throw new NoFunctionFoundException(id);
	 }
	 
	 private PTipo buscaVariavel(TId id) throws NoVariableFoundException {
		for(TiposID escopo: escopos) {
			PTipo tipo = escopo.buscaVariavel(id);
			if(tipo != null) return tipo;
		}
		
	 	throw new NoVariableFoundException(id);
	 	
	 }
	 
	 @Override
	 public void inAAtribuicaoComando(AAtribuicaoComando node) {
		 
		 PExpr expr = node.getExpr();
		 TId id = node.getId();
				 
		 
		 try {
			 PTipo tipoExpr = getTipo(expr);
			 PTipo tipoId = buscaVariavel(id);
			 
			 if (tipoExpr instanceof ATipoPrimitivoTipo && tipoId instanceof ATipoPrimitivoTipo) {
				 
				 ATipoPrimitivoTipo exprPrimitivo = (ATipoPrimitivoTipo) tipoExpr;
				 ATipoPrimitivoTipo idPrimitivo = (ATipoPrimitivoTipo) tipoId;
				 
				 if (!exprPrimitivo.getTipoPrimitivo().getClass().equals(idPrimitivo.getTipoPrimitivo().getClass())) {
					 System.out.println("ERRO");
				 }
				 
			 } else {
				 //System.out.println(id.getLine());
				 //System.out.println(id.getText());
				 //System.out.println(getTipo(expr).getClass());
				 System.out.println("incompativel");
			 }
		 } catch (NoVariableFoundException e) {
		 	System.out.println(e.getMessage());
		 }
		 
		 
		 PExpr a = node.getExpr();
		 //defaultIn(node);
	 }
	 
	 @Override
	 public void inABlocoBloco(ABlocoBloco node) {
		 List<PAtributo> atributos = node.getAtributo();
		 List<Variavel> variaveis = new ArrayList<>(atributos.size());
		 List<Constante> constantes = new ArrayList<>(atributos.size());
		 
		 for(PAtributo atributo: atributos) {
			 if(atributo instanceof ADecObjAtributo) {
				 ADecObjAtributo objs = ((ADecObjAtributo) atributo);
				 
				 for(TId id: objs.getId()) {
					 Variavel var = new Variavel(id, new ATipoClasseTipo(objs.getTipoClasse()), true); 
					 variaveis.add(var);
				 }
				 
			 } else if(atributo instanceof ADecVarAtributo) {
				 ADecVarAtributo vars = ((ADecVarAtributo) atributo);
				 
				 for(TId id: vars.getId()) {
					 Variavel var = new Variavel(id, vars.getTipo(), true); 
					 variaveis.add(var);
				 }
				 
			 } else if(atributo instanceof ADecConsAtributo) {
				 ADecConsAtributo constts = ((ADecConsAtributo) atributo);
				 
				 for(PInicializacao ini: constts.getInicializacao()) {
					 AInicializacaoInicializacao inic = ((AInicializacaoInicializacao) ini);
					 
					 Constante cons = new Constante(inic.getId(), new ATipoPrimitivoTipo(constts.getTipoPrimitivo()));
					 constantes.add(cons);
				 }
				 
			 }
		 }
		 
		 Bloco bloco = new Bloco().adicionaVariaveis(variaveis).adicionaConstantes(constantes);
		 escopos.addFirst(bloco);
		 
	 }
	 /*@Override
	 public void inABlocoExpBlocoExp(ABlocoExpBlocoExp node) {
	 	List<PAtributo> atts = node.getAtributo();
	 	List<ADecConsAtributo> cons = new ArrayList<>();
	 	
	 	for(PAtributo att: atts) {
	 		cons.add((ADecConsAtributo) att);
	 	}
	 	
	 	if(escopos.getFirst() instanceof Bloco) {
	 		Bloco atual = (Bloco) escopos.getFirst();
	 		escopos.removeFirst();
	 		atual.
	 	}
	 	
	 }*/
	 
	 @Override
	 public void inADecFuncaoMetodo(ADecFuncaoMetodo node) {

		 TId id_funcao = node.getId();
		 
		 List<PParametro> parametros = node.getParametro();
		 List<PTipo> tipos_parametros = new ArrayList<PTipo>(parametros.size());
		 List<Variavel> variaveis = new ArrayList<Variavel>(parametros.size());
		 
		 for(PParametro par: parametros) {
			 AParametroParametro param = ((AParametroParametro) par);
			 Variavel var = new Variavel(param.getId(), param.getTipo(), true);
			 tipos_parametros.add(param.getTipo());
			 variaveis.add(var);
		 }
		 
		 EstruturaSemanticaFuncao funcao = new EstruturaSemanticaFuncao(id_funcao, tipos_parametros, node.getTipo()).adicionaParametros(variaveis);
	 	 //System.out.println("Adicionando Fun��o: "+id_funcao);
	 	 ((EstruturaSemanticaClasse) escopos.getFirst()).adicionaMetodo(id_funcao, funcao);
		 escopos.addFirst(funcao);
	 	 
	 }
	 
	 @Override
	 public void inADecProcedimentoPrincipalMetodo(ADecProcedimentoPrincipalMetodo node) {
		 
		 TId id_procedimento = node.getId();
		 
		 List<PParametro> parametros = node.getParametro();
		 List<PTipo> tipos_parametros = new ArrayList<PTipo>(parametros.size());
		 List<Variavel> variaveis = new ArrayList<Variavel>(parametros.size());
		 
		 for(PParametro par: parametros) {
			 AParametroParametro param = ((AParametroParametro) par);
			 tipos_parametros.add(param.getTipo());
			 Variavel var = new Variavel(param.getId(), param.getTipo(), true);
			 variaveis.add(var);
		 }

		 EstruturaSemanticaProcedimento procedimento = new EstruturaSemanticaProcedimento(id_procedimento, tipos_parametros).adicionaParametros(variaveis);
		 //System.out.println("Adicionando Procedimento: "+id_procedimento);
		 ((EstruturaSemanticaClasse) escopos.getFirst()).adicionaMetodo(id_procedimento, procedimento);
		 escopos.addFirst(procedimento);
	 	 
	 }
	 
	 @Override
	 public void inADecProcedimentoNormalMetodo(ADecProcedimentoNormalMetodo node) {
		 TId id_procedimento = node.getId();
		 
		 List<PParametro> parametros = node.getParametro();
		 List<PTipo> tipos_parametros = new ArrayList<PTipo>(parametros.size());
		 List<Variavel> variaveis = new ArrayList<Variavel>(parametros.size());
		 
		 for(PParametro par: parametros) {
			 AParametroParametro param = ((AParametroParametro) par);
			 tipos_parametros.add(param.getTipo());
			 Variavel var = new Variavel(param.getId(), param.getTipo(), true);
			 variaveis.add(var);
		 }
		 
		 EstruturaSemanticaProcedimento procedimento = new EstruturaSemanticaProcedimento(id_procedimento, tipos_parametros).adicionaParametros(variaveis);
		 //System.out.println("Adicionando Procedimento: "+id_procedimento);
		 ((EstruturaSemanticaClasse) escopos.getFirst()).adicionaMetodo(id_procedimento, procedimento);
		 escopos.addFirst(procedimento);
		
	 }
	 
	 @Override
	 public void inADefClasseDefClasse(ADefClasseDefClasse node) {
		 TCid id_classe = node.getCid();
		 
		 List<PAtributo> atributos = node.getAtributo();
		 List<Constante> constantes = new ArrayList<Constante>(atributos.size());
		 List<Variavel> variaveis = new ArrayList<Variavel>(atributos.size());
		 
		 for(PAtributo att: atributos) {
			 if(att instanceof ADecConsAtributo) {
				 
				 ADecConsAtributo conss = ((ADecConsAtributo) att);
				 
				 ATipoPrimitivoTipo tipo = new ATipoPrimitivoTipo(conss.getTipoPrimitivo());
				 
				 List<PInicializacao> inicializacoes = conss.getInicializacao();
				 
				 for(PInicializacao ini: inicializacoes) {
					 AInicializacaoInicializacao inic = ((AInicializacaoInicializacao) ini);	
					 
					 Constante cons = new Constante(inic.getId(), tipo);
					 constantes.add(cons);
				 }
				 
			 }
			 
			 else if(att instanceof ADecVarAtributo) {

				 ADecVarAtributo vars = ((ADecVarAtributo) att);
				 PTipo tipo = vars.getTipo();
				 
				 for(TId id: vars.getId()) {
					 Variavel var = new Variavel(id, tipo, true);
					 variaveis.add(var);
				 }
			 }
			 
			 else if(att instanceof ADecObjAtributo) {

				 ADecObjAtributo objs = ((ADecObjAtributo) att);
				 
				 ATipoClasseTipo tipo = new ATipoClasseTipo(objs.getTipoClasse());
				 
				 for(TId id: objs.getId()) {
					 Variavel var = new Variavel(id, tipo, true);
					 variaveis.add(var);
				 }
			 }
			 
		 }
		 
		 EstruturaSemanticaClasse classe = new EstruturaSemanticaClasse(id_classe).adicionaVariaveis(variaveis).adicionaConstantes(constantes);
		 //System.out.println("Adicionando Classe: "+id_classe);
		 escopos.addFirst(classe);
	 }
	 
	 @Override
	 public void outADefClasseDefClasse(ADefClasseDefClasse node) {
		 ((EstruturaSemanticaClasse) escopos.getFirst()).imprime();
		 //System.out.println("Removendo :"+escopos.getFirst().getName());
		 escopos.removeFirst();
	 }
	 
     @Override
     public void outABlocoBloco(ABlocoBloco node) {
		 //System.out.println("Removendo :"+escopos.getFirst().getName());
    	 escopos.removeFirst();
     }
     
     @Override
     public void outADecProcedimentoNormalMetodo(ADecProcedimentoNormalMetodo node) {
		 //System.out.println("Removendo :"+escopos.getFirst().getName());
    	 escopos.removeFirst();
     }
     
     @Override
     public void outADecProcedimentoPrincipalMetodo(ADecProcedimentoPrincipalMetodo node) {
		 //System.out.println("Removendo :"+escopos.getFirst().getName());
    	 escopos.removeFirst();
     }
     
     @Override
     public void outADecFuncaoMetodo(ADecFuncaoMetodo node) {
		 //System.out.println("Removendo :"+escopos.getFirst().getName());
    	 escopos.removeFirst();
     }
          
}
