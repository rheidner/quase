package quase;

import quase.node.PTipo;
import quase.node.TId;

public class Constante {
	private TId id;
	private PTipo tipo;
	
	public Constante(TId id, PTipo tipo) {
		this.id = id;
		this.tipo = tipo;
	}
	
	public TId getId() {
		return id;
	}
	public void setId(TId id) {
		this.id = id;
	}
	public PTipo getTipo() {
		return tipo;
	}
	public void setTipo(PTipo tipo) {
		this.tipo = tipo;
	}
	
	
}
