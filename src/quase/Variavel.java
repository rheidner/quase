package quase;

import quase.node.PTipo;
import quase.node.TId;

public class Variavel{
	private TId id;
	private boolean inicializado;
	private PTipo tipo;
	
	public Variavel(TId id, PTipo tipo, boolean inicializado) {
		this.id = id;
		this.tipo = tipo;
		this.inicializado = inicializado;
	}
	
	public TId getId() {
		return id;
	}
	public void setId(TId id) {
		this.id = id;
	}
	public boolean isInicializado() {
		return inicializado;
	}
	public void setInicializado(boolean inicializado) {
		this.inicializado = inicializado;
	}
	public PTipo getTipo() {
		return tipo;
	}
	public void setTipo(PTipo tipo) {
		this.tipo = tipo;
	}
}
