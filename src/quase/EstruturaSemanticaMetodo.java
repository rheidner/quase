package quase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import quase.node.PTipo;
import quase.node.TId;

public abstract class EstruturaSemanticaMetodo extends TiposID {
	protected TId id;
	protected List<PTipo> tipos_parametros;
	protected Map<String, Variavel> parametros;

	public EstruturaSemanticaMetodo(TId id, List<PTipo> tipo_parametros) {
		this.id = id;
		this.tipos_parametros = tipo_parametros;
		this.parametros = new HashMap<String, Variavel>();
	}
	
	public Map<String, Variavel> getParametros() {
		return parametros;
	}

	public void setParametros(Map<String, Variavel> parametros) {
		this.parametros = parametros;
	}
	
	public TId getId() {
		return id;
	}
	public void setId(TId id) {
		this.id = id;
	}
	public List<PTipo> getTipos_parametros() {
		return tipos_parametros;
	}
	public void setTipos_parametros(List<PTipo> tipos_parametros) {
		this.tipos_parametros = tipos_parametros;
	}
	
	protected EstruturaSemanticaMetodo adicionaParametro(Variavel var) {
		parametros.put(var.getId().getText(), var);
		return this;
	}
	
	public EstruturaSemanticaMetodo adicionaParametros(List<Variavel> vars) {
		for(Variavel var: vars)
			adicionaParametro(var);
		
		return this;
	}
	
	protected void imprimeIndent(int indent) {
		for(int i = 0; i < indent; i++)
			System.out.print("\u0009");
	}
	
	protected void imprimeParametros(int indent) {
		imprimeIndent(indent);
		System.out.println("Parametros:");
		for(String key: parametros.keySet()) {
			imprimeIndent(indent+1);
			System.out.println("Id: "+key+" Tipo: "+parametros.get(key).getTipo());
		}
		System.out.println();
	}
	
	public abstract void imprime(int ident);
}
