/* This file was generated by SableCC (http://www.sablecc.org/). */

package quase.node;

import java.util.*;
import quase.analysis.*;

@SuppressWarnings("nls")
public final class ADecVarAtributo extends PAtributo
{
    private PTipo _tipo_;
    private final LinkedList<TId> _id_ = new LinkedList<TId>();

    public ADecVarAtributo()
    {
        // Constructor
    }

    public ADecVarAtributo(
        @SuppressWarnings("hiding") PTipo _tipo_,
        @SuppressWarnings("hiding") List<TId> _id_)
    {
        // Constructor
        setTipo(_tipo_);

        setId(_id_);

    }

    @Override
    public Object clone()
    {
        return new ADecVarAtributo(
            cloneNode(this._tipo_),
            cloneList(this._id_));
    }

    public void apply(Switch sw)
    {
        ((Analysis) sw).caseADecVarAtributo(this);
    }

    public PTipo getTipo()
    {
        return this._tipo_;
    }

    public void setTipo(PTipo node)
    {
        if(this._tipo_ != null)
        {
            this._tipo_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._tipo_ = node;
    }

    public LinkedList<TId> getId()
    {
        return this._id_;
    }

    public void setId(List<TId> list)
    {
        this._id_.clear();
        this._id_.addAll(list);
        for(TId e : list)
        {
            if(e.parent() != null)
            {
                e.parent().removeChild(e);
            }

            e.parent(this);
        }
    }

    @Override
    public String toString()
    {
        return ""
            + toString(this._tipo_)
            + toString(this._id_);
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        if(this._tipo_ == child)
        {
            this._tipo_ = null;
            return;
        }

        if(this._id_.remove(child))
        {
            return;
        }

        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        if(this._tipo_ == oldChild)
        {
            setTipo((PTipo) newChild);
            return;
        }

        for(ListIterator<TId> i = this._id_.listIterator(); i.hasNext();)
        {
            if(i.next() == oldChild)
            {
                if(newChild != null)
                {
                    i.set((TId) newChild);
                    newChild.parent(this);
                    oldChild.parent(null);
                    return;
                }

                i.remove();
                oldChild.parent(null);
                return;
            }
        }

        throw new RuntimeException("Not a child.");
    }
}
