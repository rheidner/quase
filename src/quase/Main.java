 package quase;
import java.io.*;

import quase.analysis.DepthFirstAdapter;
import quase.lexer.Lexer;
import quase.node.EOF;
import quase.node.Start;
import quase.node.Token;
import quase.parser.Parser;



public class Main
{
	public static void main(String[] args)
	{
		try
		{
			String arquivo = "teste/teste.txt";

			/*Lexer lexer =
					new Lexer(
							new PushbackReader(  
									new FileReader(arquivo), 1024)); 
			Token token;
			while(!((token = lexer.next()) instanceof EOF)) {
				System.out.print(token.getClass());
				System.out.println(" ( "+token.toString()+")");
			}*/
			
		   Parser p =
				    new Parser(
				    new Lexer(
				    new PushbackReader(  
				    new FileReader(arquivo), 1024))); 
				   
		   	Start tree = p.parse();

		   tree.apply(new Semantico());
			
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
}