/* This file was generated by SableCC (http://www.sablecc.org/). */

package quase.node;

import java.util.*;
import quase.analysis.*;

@SuppressWarnings("nls")
public final class ADefClasseDefClasse extends PDefClasse
{
    private TCid _cid_;
    private final LinkedList<PAtributo> _atributo_ = new LinkedList<PAtributo>();
    private final LinkedList<PMetodo> _metodo_ = new LinkedList<PMetodo>();

    public ADefClasseDefClasse()
    {
        // Constructor
    }

    public ADefClasseDefClasse(
        @SuppressWarnings("hiding") TCid _cid_,
        @SuppressWarnings("hiding") List<PAtributo> _atributo_,
        @SuppressWarnings("hiding") List<PMetodo> _metodo_)
    {
        // Constructor
        setCid(_cid_);

        setAtributo(_atributo_);

        setMetodo(_metodo_);

    }

    @Override
    public Object clone()
    {
        return new ADefClasseDefClasse(
            cloneNode(this._cid_),
            cloneList(this._atributo_),
            cloneList(this._metodo_));
    }

    public void apply(Switch sw)
    {
        ((Analysis) sw).caseADefClasseDefClasse(this);
    }

    public TCid getCid()
    {
        return this._cid_;
    }

    public void setCid(TCid node)
    {
        if(this._cid_ != null)
        {
            this._cid_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._cid_ = node;
    }

    public LinkedList<PAtributo> getAtributo()
    {
        return this._atributo_;
    }

    public void setAtributo(List<PAtributo> list)
    {
        this._atributo_.clear();
        this._atributo_.addAll(list);
        for(PAtributo e : list)
        {
            if(e.parent() != null)
            {
                e.parent().removeChild(e);
            }

            e.parent(this);
        }
    }

    public LinkedList<PMetodo> getMetodo()
    {
        return this._metodo_;
    }

    public void setMetodo(List<PMetodo> list)
    {
        this._metodo_.clear();
        this._metodo_.addAll(list);
        for(PMetodo e : list)
        {
            if(e.parent() != null)
            {
                e.parent().removeChild(e);
            }

            e.parent(this);
        }
    }

    @Override
    public String toString()
    {
        return ""
            + toString(this._cid_)
            + toString(this._atributo_)
            + toString(this._metodo_);
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        if(this._cid_ == child)
        {
            this._cid_ = null;
            return;
        }

        if(this._atributo_.remove(child))
        {
            return;
        }

        if(this._metodo_.remove(child))
        {
            return;
        }

        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        if(this._cid_ == oldChild)
        {
            setCid((TCid) newChild);
            return;
        }

        for(ListIterator<PAtributo> i = this._atributo_.listIterator(); i.hasNext();)
        {
            if(i.next() == oldChild)
            {
                if(newChild != null)
                {
                    i.set((PAtributo) newChild);
                    newChild.parent(this);
                    oldChild.parent(null);
                    return;
                }

                i.remove();
                oldChild.parent(null);
                return;
            }
        }

        for(ListIterator<PMetodo> i = this._metodo_.listIterator(); i.hasNext();)
        {
            if(i.next() == oldChild)
            {
                if(newChild != null)
                {
                    i.set((PMetodo) newChild);
                    newChild.parent(this);
                    oldChild.parent(null);
                    return;
                }

                i.remove();
                oldChild.parent(null);
                return;
            }
        }

        throw new RuntimeException("Not a child.");
    }
}
